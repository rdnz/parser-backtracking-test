module MegaTest where

import Text.Megaparsec
import Text.Megaparsec.Byte -- qualified as P
import Relude

type Parser = Parsec Void ByteString

test0 :: Either (ParseErrorBundle ByteString Void) Word8
test0 =
  testParser $
    (char 97 *> char 97 *> char 97 <|> char 97 *> char 97)

-- $> MegaTest.test0

-- Left (ParseErrorBundle {bundleErrors = TrivialError 2 (Just EndOfInput) (fromList [Tokens (97 :| [])]) :| [], bundlePosState = PosState {pstateInput = "aa", pstateOffset = 0, pstateSourcePos = SourcePos {sourceName = "", sourceLine = Pos 1, sourceColumn = Pos 1}, pstateTabWidth = Pos 8, pstateLinePrefix = ""}})

test1 :: Either (ParseErrorBundle ByteString Void) Word8
test1 =
  testParser $
    (try (char 97 *> char 97 *> char 97) <|> char 97 *> char 97)

-- $> MegaTest.test1

-- Right 97

test2 :: Either (ParseErrorBundle ByteString Void) Word8
test2 =
  testParser $
    try (try (char 97 *> char 97) <|> try (char 97)) *> try (char 97)

-- $> MegaTest.test2

-- Left (ParseErrorBundle {bundleErrors = TrivialError 2 (Just EndOfInput) (fromList [Tokens (97 :| [])]) :| [], bundlePosState = PosState {pstateInput = "aa", pstateOffset = 0, pstateSourcePos = SourcePos {sourceName = "", sourceLine = Pos 1, sourceColumn = Pos 1}, pstateTabWidth = Pos 8, pstateLinePrefix = ""}})

testParser :: Parser a -> Either (ParseErrorBundle ByteString Void) a
testParser parser = runParser parser "" "aa"
