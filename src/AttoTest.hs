module AttoTest where

import Data.Attoparsec.ByteString
import Relude

test0 :: Either String Word8
test0 =
  testParser $
    (word8 97 *> word8 97 *> word8 97 <|> word8 97 *> word8 97)

-- $> AttoTest.test0

-- Right 97

test1 :: Either String Word8
test1 =
  testParser $
    (try (word8 97 *> word8 97 *> word8 97) <|> word8 97 *> word8 97)

-- $> AttoTest.test1

-- Right 97

test2 :: Either String Word8
test2 =
  testParser $
    try (try (word8 97 *> word8 97) <|> try (word8 97)) *> try (word8 97)

-- $> AttoTest.test2

-- Left "97: not enough input"

testParser :: Parser a -> Either String a
testParser parser = parseOnly parser "aa"
