module Main where

import MegaTest qualified
import AttoTest qualified
import Relude

main :: IO ()
main =
  print MegaTest.test0 *>
  print MegaTest.test1 *>
  print MegaTest.test2 *>
  print AttoTest.test0 *>
  print AttoTest.test1 *>
  print AttoTest.test2
